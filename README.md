# README for Dice Rolling Simulator

## Introduction
This Dice Rolling Simulator is a simple Python program that allows users to roll a set number of dice. It's a fun and useful tool for anyone who needs random numbers, particularly for tabletop games or educational purposes.

## Features
- Roll any number of dice (default is 2).
- Customizable dice amount.
- Error handling for invalid inputs.
- Continuous rolling until the user decides to exit.

## Requirements
- Python 3.x

## Installation
No additional installation is required. The script runs with Python 3.x, which should be pre-installed on most systems.

## Usage
To use the Dice Rolling Simulator:
1. Run the script in a Python environment.
2. When prompted, input the number of dice you want to roll. The default is 2.
3. To exit the program, type 'exit'.

## How it Works
1. The `roll_dice` function generates a list of random numbers, each representing a dice roll.
2. The `main` function handles user input and displays the results of the dice rolls.
3. Error handling is in place for non-numeric or invalid inputs.

## Example
```bash
How many dice would you like to roll? 3
4, 2, 6
```


## Contributions
Contributions, bug reports, and feature requests are welcome. Please open an issue or submit a pull request on the repository.

## License
This project is open-source and available under the [MIT License](https://opensource.org/licenses/MIT).

---
